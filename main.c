/*
 * MIT License
 * 
 * Copyright (c) 2016 wen.gu <454727014@qq.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
 

#include <stdio.h>
#include "tiny_ini_file.h"

#define LOGD printf
#define LOGE printf

#define TEST_INI_FILE_NAME "my_test.ini"

int main(int argc, char *argv[])
{
	GS32 ret = G_OK;
	tiny_ini_file_t* ini_file = NULL;
	
	ret = TinyIniFileCreate(&ini_file);
	
	if (G_OK == ret)
	{
		ret = ini_file->load(ini_file, TEST_INI_FILE_NAME);
		
		if (G_OK == ret)
		{
			char* value = NULL;
			
			ini_file->dump(ini_file); /** print the content of ini file */
			
			LOGD("=========this is cut-off rule===============\n\n");
			LOGD("start test get value and get values:\n\n");
			ret = ini_file->getValue(ini_file, DEFAULT_SECTION_NAME, "app_name", &value);
			
			if (G_OK == ret)
			{
				LOGD("section: %s, key: %s, value:%s\n", DEFAULT_SECTION_NAME, "app_name", value);
			}
			
			{
				GU32 i;
				char** values = NULL;
				GU32  values_cnt = 0;
				
				ret = ini_file->getValues(ini_file, "test_section", "test_key", &values, &values_cnt);
				
				if (G_OK == ret)
				{
					for (i = 0; i < values_cnt; i++)
					{
						LOGD("section: %s, key: %s, value[%d]:%s\n", "test_section", "test_key", i, values[i]);
					}
					
					/** when use up, valuse, need do free with API freeValues() */					
					ini_file->freeValues(ini_file, values, values_cnt);
				}
				
			}			
			
		}
		else
		{
			LOGE("load ini file(%s) failed(0x%08x)\n", TEST_INI_FILE_NAME, ret);
		}
	}
	else
	{
		LOGE("create ini file instance failed(0x%08x)\n", ret);
	}
	
	if (ini_file)
	{
		ini_file->destroy(ini_file);
	}   

    return 0;
}
